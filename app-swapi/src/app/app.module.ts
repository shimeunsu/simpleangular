import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SwapiHomeComponent } from './swapi-home/swapi-home.component';
import { SwapiSearchComponent } from './swapi-search/swapi-search.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SwapiDocsComponent } from './swapi-docs/swapi-docs.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SwapiHomeComponent,
    SwapiSearchComponent,
    PageNotFoundComponent,
    SwapiDocsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
