import { Component, OnInit, Query } from '@angular/core';
import { SwapiDatabaseService } from '../services/swapi-database.service'
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-swapi-search',
  templateUrl: './swapi-search.component.html',
  styleUrls: ['./swapi-search.component.css']
})
export class SwapiSearchComponent implements OnInit {
  options = [
    "People",
    "Planet",
    "Species",
    "Starship",
    "Vehicle"
  ]
  numbers = [
    1,
    2,
    3,
    4,
    5
  ]
  option = new FormControl('')
  number = new FormControl('')
  queued:boolean = false;
  query() {
    switch(this.option.value){
      case "People":
        this.getPeople(this.number.value);
        break;
      case "Planet":
        this.getPlanet(this.number.value);
        break;
      case "Species":
        this.getSpecies(this.number.value);
        break;
      case "Starship":
        this.getStarship(this.number.value);
        break;
      case "Vehicle":
        this.getVehicle(this.number.value);
        break;
    }
    this.queued=true;
  }

  item;
  constructor(private swapiDatabaseService:SwapiDatabaseService) { }
  ngOnInit() { }

  getPeople(ID){
    this.swapiDatabaseService.getPeopleByID(ID)
      .subscribe( (info) => {
        this.item = info;
    } );
  }

  getPlanet(ID){
    this.swapiDatabaseService.getPlanetByID(ID)
    .subscribe( (info) => {
      this.item = info;
    } );
  }
  
  getSpecies(ID){
    this.swapiDatabaseService.getSpeciesByID(ID)
    .subscribe( (info) => {
      this.item = info;
    } );
  }

  getStarship(ID){
    this.swapiDatabaseService.getStarshipByID(ID)
    .subscribe( (info) => {
      this.item = info;
    } );
  }

  getVehicle(ID){
    this.swapiDatabaseService.getVehicleByID(ID)
    .subscribe( (info) => {
      this.item=info;
    } );
  }

}
