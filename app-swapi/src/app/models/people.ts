import { Vehicle } from './vehicle';

import { Species } from './species';

import { Starship } from './starship';

export class People {
    name: string;
    height: number;
    mass: number; 
    hair_color: string; 
    eye_color: string; 
    skin_color: string; 
    birth_year: string;
    gender: string;
    homeworld: string;
    
    speices: Species;
    vehicles: Vehicle;
    starships: Starship;

    created: string;
    edited: string;
    url: string;
}