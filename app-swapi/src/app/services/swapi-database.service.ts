import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { People } from '../models/people';
import { Planet } from '../models/planet';
import { Species } from '../models/species';
import { Starship } from '../models/starship';
import { Vehicle } from '../models/vehicle';


@Injectable({
  providedIn: 'root'
})
export class SwapiDatabaseService {
  
  dbBaseUrl = "https://swapi.co/api";
  SWAPI = {
    peo: "people",
    pla: "planets",
    spe: "species",
    sta: "starships",
    veh: "vehicles"
  };
  constructor(private http: HttpClient) { }

  getPeopleByID(ID):Observable<People> {
    return this.http.get<People>(`${this.dbBaseUrl}/${this.SWAPI.peo}/${ID}`);
  }
  getPlanetByID(ID):Observable<Planet> {
    return this.http.get<Planet>(`${this.dbBaseUrl}/${this.SWAPI.pla}/${ID}`);
  }
  getSpeciesByID(ID):Observable<Species> {
    return this.http.get<Species>(`${this.dbBaseUrl}/${this.SWAPI.spe}/${ID}`);
  }
  getStarshipByID(ID):Observable<Starship> {
    return this.http.get<Starship>(`${this.dbBaseUrl}/${this.SWAPI.sta}/${ID}`);
  }
  getVehicleByID(ID):Observable<Vehicle> {
    return this.http.get<Vehicle>(`${this.dbBaseUrl}/${this.SWAPI.veh}/${ID}`);
  }
}
