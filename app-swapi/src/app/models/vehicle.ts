export class Vehicle {
    cargo_capacity: number;
    consumables: string;
    cost_in_credits: number;
    crew: number;
    length: number;
    manufacturer: string;
    max_atmosphering_speed: number;
    model: string;
    name: string;
    vehicle_class: string
    passengers: number;
    pilots: string[];
    
    created: string;
    edited: string;
    url: string;
}