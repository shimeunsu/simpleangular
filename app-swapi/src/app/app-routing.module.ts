import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SwapiHomeComponent } from './swapi-home/swapi-home.component';
import { SwapiDocsComponent } from './swapi-docs/swapi-docs.component';
import { SwapiSearchComponent } from './swapi-search/swapi-search.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', component: SwapiHomeComponent},
  {path: 'docs', component: SwapiDocsComponent},
  {path: 'search', component: SwapiSearchComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
